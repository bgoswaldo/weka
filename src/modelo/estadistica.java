package modelo;

import java.util.Arrays;
import java.util.Comparator;

public class estadistica {
	
	public int ATTR 		= 0;
	public int MEDIA        = 1;
	public int MEDIANA      = 2;
	public int MODA         = 3;
	public int VARIANZA     = 4;
	public int DESVESTANDAR = 5;
	public int MIN          = 6;
	public int MAX          = 7;
	public int TAMTOTAL     = 8;

	public estadistica() {
		// TODO Auto-generated constructor stub
	}

	public String[] applyOperations(String fila[])
	 {
		String result[] = new String[TAMTOTAL];
		double valor[] = new double[fila.length];
		for (int i = 0; i < valor.length; i++) {
			valor[i] = Double.parseDouble(fila[i]);
		}
		
		Arrays.sort(valor);
		
		/*  
		 * +++MODA+++
		 * Para obtener la moda es necesario ordenar los datos
		 * y saber la frecuencia de estos
		 * el numero que se repita mas veces es el modal
		 * ej. 2,3,4,4,5 es sencillo decir que la moda es 4
		 * pero en ej. 2,3,3,5,6,6,7 se dice que es multimodal lo cual es 3,6
		 *
		 * */
		double[][] repeticiones = new double[valor.length][2];
		int totalDeValoresDiferentes = 0;
		for (int i = 0; i < valor.length; i++) {
			
			if(i!=0 && valor[i]!=valor[i-1])
			{
				totalDeValoresDiferentes++;
			}
			repeticiones[totalDeValoresDiferentes][0] = valor[i];
			repeticiones[totalDeValoresDiferentes][1] += 1;
		}
		
		Arrays.sort(repeticiones, new Comparator<double[]>() {
		    @Override
		    public int compare(double[] ele1, double[] ele2) {
		        return Double.compare(ele2[1], ele1[1]);
		    }
		});
		//En esta posicion se encontrara el valor con mayor repeticiones
		result[MODA] = Double.toString(repeticiones[0][0]);
		for (int i = 0; i < repeticiones.length-1; i++) {
			
			if (repeticiones[i][1] == repeticiones[i+1][1]) {
				result[MODA] = result[MODA] + "," + Double.toString(repeticiones[i+1][0]);
			}
			else
			{
				break;
			}
		}

		/*
		 * +++MEDIA+++
		 * Se suman todos los numeros y se divide
		 * entre la cantidad de valores
		 */
		float sumTotal = 0;
		for (int i = 0; i < repeticiones.length; i++) {
			sumTotal+= repeticiones[i][0] * repeticiones[i][1];
		}
		float media = sumTotal/valor.length;
		result[MEDIA] = String.format("%.02f", media);
		
		/*
		 * +++MEDIANA+++
		 * Para obtener la mediana se ordenan los valores y el valor que queda
		 * a medias es la mediana en caso de tener 4 valores 
		 * 2,3,15,20
		 * entonces se tomas los dos de a medias se suman y dividen
		 * */
		int posicion1 = fila.length/2;
		int posicion2;
		double mediana;
		if(fila.length%2==0)
		{	
			 posicion2 = posicion1-1;
			 mediana = (valor[posicion1]+ valor[posicion2])/2;
		}else
		{
			mediana = valor[posicion1];
		}
		result[MEDIANA] = Double.toString(mediana);
		
		/*
		 * +++VARIANZA+++
		 * Es la diferencia respecto de la media
		 * se hace la suma de las diferencias respecto de la media
		 * */
		double varianza = 0.0;
		for(int i = 0 ; i<valor.length; i++){
			double rango;
			rango = Math.pow(valor[i]-media,2f);
			varianza = varianza + rango;
		}
		varianza = varianza/valor.length;
		result[VARIANZA] = String.format("%.02f", varianza);
		
		/*
		 * +++DESVIACION ESTANDAR+++
		 * Para obtener la desviacion solo tenemos que sacar la raiz cuadrada de la varianza
		 * */
		double desviacion = 0.0;
		desviacion = Math.sqrt(varianza);
		result[DESVESTANDAR] = String.format("%.02f", desviacion);
		
		/*
		 * +++VALOR MIN+++
		 * */
		result[MIN]= Double.toString(valor[0]);
		/*
		 * +++VALOR MAX+++
		 * */
		result[MAX]= Double.toString(valor[valor.length - 1]);
		return result;
	 }
}
