package modelo;

import java.util.Arrays;

public class transformaciones 
{

	public transformaciones() 
	{
		// TODO Auto-generated constructor stub
	}

	public Object[] numericos(String datos[])
	{
		double numeros[] = new double[datos.length];
		double temp[] = new double[datos.length];
		Object[] temp1 = new Object[datos.length];
		
		for (int i = 0; i < datos.length; i++)
		{
			numeros[i] = temp[i] = Double.parseDouble(datos[i]);
		}
		
		Arrays.sort(temp);
				
		for (int i = 0; i < numeros.length; i++) 
		{
			temp1[i] = Math.abs((numeros[i] - temp[0])/(numeros[temp.length-1] - numeros[0]));
		}
		
		return temp1;
	}
        
	private int getDistLev(String s, String t)
	{
		int n = s.length();
		int m = t.length();
		int[][] d = new int[n + 1][ m + 1];
	
		if (n == 0)
		{
			return m;
		}
	
		if (m == 0)
		{
			return n;
		}
	
		for (int i = 0; i <= n; d[i][0] = i++)
		{ }
	
		for (int j = 0; j <= m; d[0][j] = j++)
		{ }
	
		for (int i = 1; i <= n; i++)
		{
			for (int j = 1; j <= m; j++)
			{                
				int costo;
				
				if(t.charAt(j-1) == s.charAt(i-1))                        
					costo = 0;
				else
					costo = 1;              
				
				d[i][j] = Math.min(Math.min(d[i-1][j]+1,d[i][j-1]+1),
				d[i-1][j-1] + costo);
			}
		}
		return d[n][m];
	}

	public String getDistanciaMinima(String texto, String patron)
	{
		String[] tokens = texto.split("-");
		String cadmin = "";
		int distminima = 99999;

		for(int i = 0; i < tokens.length; i++)
		{
			//obtenemos la distancia por cada palabra del texto total
			int dist = this.getDistLev(tokens[i], patron);
			if(dist < distminima)
			{
				distminima = dist;
				cadmin = tokens[i];
			}          
		}
		
		return cadmin;
	}
}
