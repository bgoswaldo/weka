package modelo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class expresiones 
{
	String[] expresionesRegulares;
	String[] valoresInvalidos;
	String[] posicionesDeDatosInvalidos;
	int[] cantidadValoresInvalidos;
	
	public expresiones(int tam, String nombre) 
	{
		expresionesRegulares = getExpresiones(tam, nombre);
		valoresInvalidos = new String[expresionesRegulares.length];
		cantidadValoresInvalidos = new int[expresionesRegulares.length];
		posicionesDeDatosInvalidos = new String[expresionesRegulares.length];
	}
	
	public expresiones() 
	{
		
	}
	
	public void crearArchivo(int tam, String nombre)
	{
		File temp = new File("src/configuracion/"+nombre.substring(0,(nombre.length()-3))+"txt");
		BufferedWriter bw = null;
		
		try {
			temp.createNewFile();
			bw = new BufferedWriter(new FileWriter(temp));
	    	for (int i = 0; i < tam; i++) 
	    	{
   				bw.write("\n");
			}
	    	bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
	}
	
	public boolean existe(String nombre)
	{
		File temp = new File("src/configuracion/"+nombre.substring(0,(nombre.length()-3))+"txt");
		
		return temp.exists();
	}

	public void setExpresiones(int tam, String nombre,int col, String exp)
	{
		File temp = new File("src/configuracion/"+nombre.substring(0,(nombre.length()-3))+"txt");
		File tempW = new File("src/configuracion/"+nombre.substring(0,(nombre.length()-4))+"temp.txt");
		FileReader fr = null;
	    BufferedWriter bw = null;
	    
	    if(temp.exists() == true)
	    {
	    	String mod[] = getExpresiones(tam, nombre);
	    	try 
	    	{
	    		tempW.createNewFile();
	    		bw = new BufferedWriter(new FileWriter(tempW));
	    		for (int i = 0; i < tam; i++) 
	    		{
	    			if(i == col)
	    			{
	    				bw.write(exp + "\n");
	    			}
	    			else
	    			{
	    				bw.write(mod[i] + "\n");
	    			}
				}
	    		bw.close();
	    		temp.delete();
	    		tempW.renameTo(temp);
	    	}
		    catch(Exception e){
		         e.printStackTrace();
		    }finally{
		         try{                    
		            if(null != fr)  
		               fr.close();                       
		         }catch (Exception e2){ 
		            e2.printStackTrace();
		         }
		    }
	    }
	}
	
	public String[] getExpresiones(int tam, String nombre)
	{
		String exp[] = new String[tam];
		File temp = new File("src/configuracion/"+nombre.substring(0,(nombre.length()-3))+"txt");
	    FileReader fr = null;
	    BufferedReader br = null;
	    
	    if(temp.exists() == true)
	    {
	    	try {
		        fr = new FileReader (temp);
		        br = new BufferedReader(fr);
		        
		        int i = 0;
		        
		        while (i < tam) 
		        {
		        	exp[i] = br.readLine();
		        	i++;
				}
		        fr.close();
		    }
		    catch(Exception e){
		         e.printStackTrace();
		    }finally{
		         try{                    
		            if(null != fr)  
		               fr.close();                       
		         }catch (Exception e2){ 
		            e2.printStackTrace();
		         }
		    }
	    }else
	    {
	    	int i = 0;
	    	while(i < tam)
	    	{
	    		exp[i] = "";
	    		i++;
	    	}
	    }
		return exp;
	}
	
	public void restaurarExp(String nombre)
	{
		File temp = new File("src/configuracion/"+nombre.substring(0,(nombre.length()-3))+"txt");
		File tempD = new File("src/configuracion/"+nombre.substring(0,(nombre.length()-4))+"delete.txt");
		
		if(tempD.exists())
		{
			temp.delete();
			tempD.renameTo(temp);
		}
	}
	
	public void guardarExp(String nombre, String nombre2)
	{
		File tempD = new File("src/configuracion/"+nombre.substring(0,(nombre.length()-4))+"delete.txt");
		File tempG = new File("src/configuracion/"+nombre.substring(0,(nombre.length()-4))+".txt");
		File tempG2 = new File("src/configuracion/"+nombre2.substring(0,(nombre2.length()-4))+".txt");
		
		if(tempG.exists())
			tempG.renameTo(tempG2);
		
		tempD.delete();
	}
	
	public void eliminar(String nombre)
	{
		File temp = new File("src/configuracion/"+nombre.substring(0,(nombre.length()-3))+"txt");
		File tempD = new File("src/configuracion/"+nombre.substring(0,(nombre.length()-4))+"delete.txt");
		
		temp.renameTo(tempD);
	}
	
	public void eliminarExp(int tam, String nombre,int col)
	{
		File tempW = new File("src/configuracion/"+nombre.substring(0,(nombre.length()-3))+"txt");

		FileReader fr = null;
	    BufferedWriter bw = null;
	    
//	    if(tempW.exists() == true)
//	    {
	    	String mod[] = getExpresiones(tam, nombre.substring(0,(nombre.length()-4))+"delete.txt");
	    	//tempW.delete();
	    	
	    	try 
	    	{
	    		tempW.createNewFile();
	    		
	    		bw = new BufferedWriter(new FileWriter(tempW));
	    		for (int i = 0; i < tam; i++) 
	    		{
	    			if(i != col)
	    			{
	    				bw.write(mod[i]);
	    				//System.out.println(mod[i] + "\n");
	    				if(i+1 < tam)
	    					bw.write("\n");
	    			}
				}
	    		bw.close();
	    	}
		    catch(Exception e){
		         e.printStackTrace();
		    }finally{
		         try{                    
		            if(null != fr)  
		               fr.close();                       
		         }catch (Exception e2){ 
		            e2.printStackTrace();
		         }
//		    }
	    }
	}
	
	public boolean validateValue(int column, int row, String value, int tam) {
		boolean result = false;
		if(value.matches(expresionesRegulares[column]))
		{
			result = true;
		}
		else
		{
			cantidadValoresInvalidos[column] += 1;
			if (cantidadValoresInvalidos[column] > 1) 
			{
				posicionesDeDatosInvalidos[column] += ", "+ (row+1)  ;
				valoresInvalidos[column] += "\n "+ value;
			}else
			{
				posicionesDeDatosInvalidos[column] = (row + 1) + "";
				valoresInvalidos[column] = value;
			}
		}
		return result;
	}
	
	public String[] getExpresionesRegulares()
	{
		return expresionesRegulares;
	}
	
	public String[] getValoresInvalidos()
	{
		return valoresInvalidos;
	}
	
	public String[] getPosicionesDeDatosInvalidos()
	{
		return posicionesDeDatosInvalidos;
	}
	
	public int[] getCantidadValoresInvalidos()
	{
		return cantidadValoresInvalidos;
	}
}
