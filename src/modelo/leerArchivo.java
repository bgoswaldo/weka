package modelo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

public class leerArchivo {

	public leerArchivo() 
	{
		
	}
	
	private String url(String url)
	{
		return url = url.replace("\\", "/");
	}
	
	public String[] encabezado(String url)
	{
		String list[] = null;
		try {     
	        url = url(url);
	         
	        CsvReader leer = new CsvReader(url);
	       
	        if(leer.readRecord() == true)
	        {
	        	list = new String[leer.getColumnCount()];
	        	list = leer.getValues();
	        }
	         
	        leer.close();

	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		return list;
	}
	
	public int getRow(String url)
	{
		int count = 0;
		
		try {     
	        url = url(url);
	         
	        CsvReader leer = new CsvReader(url);
	       
	        if(leer.readRecord() == true)
	        {	
	        	while (leer.readRecord()) 
	        	{
	        		count++;
	            }
	        }
	         
	        leer.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		
		return count;
	}
	
	public String[] mostrar(String url)
	{
		String[] temp = null;
		
		try {     
	        url = url(url);
	         
	        CsvReader leer = new CsvReader(url);
	       
	        if(leer.readRecord() == true)
	        {
	        	temp = new String[leer.getColumnCount()*getRow(url)];
	        	int i = 0;
	        	while (leer.readRecord()) {
	        		for (int j = 0; j < leer.getColumnCount(); j++) 
	        		{
	        			temp[i] = leer.get(j);
	        			i++;
					}
	            }
	        }
	         
	        leer.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		
		return temp;
	}
	
	public ArrayList<archivo> valores(String url, int columna)
	{
		ArrayList<archivo> archivo = new ArrayList<archivo>();
		
		try {     
	        url = url(url);
	         
	        CsvReader leer = new CsvReader(url);
	       
	        while (leer.readRecord())
	        {
	            String nombre = leer.get(columna);
	            if(archivo.size() == 0)
	            {
	            	archivo a = new archivo(); 
		            a.valores(nombre, 1);
		            archivo.add(a);
	            }else
	            {
	            	boolean band = false;
	            	
	            	for(archivo ar : archivo)
	            	{
	            		if(nombre.equals(ar.nombre))
	            		{
	            			ar.repeticion++;
	            			band = true;
	            			break;
	            		}
	            	}
	            	if(band == false)
            		{
            			archivo a = new archivo(); 
    		            a.valores(nombre, 1);
    		            archivo.add(a);
            		}
	            }
	        }
	         
	        leer.close();

	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		return archivo;
	}
	
	public void guardar(String columnas[], String filas[])
	{
		String path = "/temp";
		URL ruta = this.getClass().getResource(path);
		File fichero = new File(ruta.getPath()+"/temp.csv");
		fichero.delete();
		
		try 
		{
			CsvWriter escribir = new CsvWriter(new FileWriter(ruta.getPath()+"/temp.csv",true),',');
			
			for (int i = 0; i < columnas.length; i++) 
			{
				escribir.write(columnas[i]);
			}
			escribir.endRecord();
			
			int j = 0;
			for (int i = 0; i < filas.length; i++) 
			{
				escribir.write(filas[i]);
				j++;
				if(j >= columnas.length)
				{
					escribir.endRecord();
					j = 0;
				}
			}
			escribir.close();
			
		} catch (IOException ioe) 
		{
			// TODO: handle exception
		}
	}
	
	public void restaurar()
	{
		String path = "/temp";
		URL ruta = this.getClass().getResource(path);
		File fichero = new File(ruta.getPath()+"/temp.csv");
		fichero.delete();
	}
}
