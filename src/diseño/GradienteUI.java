package dise�o;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

public class GradienteUI extends BasicTabbedPaneUI
{
	private Color color1 = new Color(0,0,0);
    private Color color2 = new Color(0,0,0);
      
    public GradienteUI(Color c1, Color c2){
        color1 = c1;
        color2 = c2;        
    }
	public void paintTabBackground(Graphics g, int tabPlacement, int tabindex, int x, int y, int w, int h, boolean isSelected)
	{
		Graphics2D g2 = (Graphics2D)g;
		GradientPaint gradiente = new GradientPaint(x, y+h/2, color2, x+w, y+h/2, color1.brighter(), true),
					  selected = new GradientPaint(x, y+h/2, color1, x+w, y+h/2, color1, true);
		g2.setPaint(!isSelected ? gradiente : selected);
		switch (tabPlacement) {
		case LEFT:
            g.fillRect(x+1, y+1, w-1, h-3);
            break;
        case RIGHT:
            g.fillRect(x, y+1, w-2, h-3);
            break;
        case BOTTOM:
            g.fillRect(x+1, y, w-3, h-1);
            break;
        case TOP:
        default:
            g.fillRect(x+1, y+1, w-3, h-1);
        break;
		}
	}
}
