package dise�o;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JLabel;

public class JUrlLabel extends JLabel implements MouseListener {

	private static final long serialVersionUID = 1L;
	
	private URI url;
	
	public JUrlLabel(String titulo, String direccion) 
	{
		try {
			url = new URI(direccion);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		setText(titulo);
		setForeground(new Color(0, 191, 255));
        setToolTipText(url.toString());
        setCursor(new Cursor(Cursor.HAND_CURSOR));
        addMouseListener(this);
	}
	
	public JUrlLabel(String titulo) 
	{
		setText(titulo);
		setForeground(new Color(0, 191, 255));
        setCursor(new Cursor(Cursor.HAND_CURSOR));
        addMouseListener(this);
	}
	
	public JUrlLabel() 
	{
        setCursor(new Cursor(Cursor.HAND_CURSOR));
        setForeground(new Color(0, 191, 255));
        addMouseListener(this);
	}
	
	public void setUrl(String direccion)
    {
        try {
            url = new URI(direccion);
            setToolTipText(direccion);
        } catch (URISyntaxException ex) {
            System.err.println( ex.getMessage() );
        }
    }
	
	public String getURL()
    {
        return url.toString();
    }
	
	@Override
	   protected void paintComponent(Graphics g) {
	        super.paintComponent(g);
	        Rectangle r;            
	        r = g.getClipBounds();            
	        g.drawLine(0, r.height - getFontMetrics(getFont()).getDescent(), getFontMetrics(getFont())
	                .stringWidth(getText()), r.height - getFontMetrics(getFont()).getDescent());        
	  }

	@Override
	public void mouseClicked(MouseEvent arg0) {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) 
        {
            try {
                desktop.browse( url );
            } catch ( Exception ex ) {
                System.err.println( ex.getMessage() );
            }
        }    
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
