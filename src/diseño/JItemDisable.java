package dise�o;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

public class JItemDisable {

	Object obj;

    boolean isEnable;

    public JItemDisable(Object obj, boolean isEnable) {
      this.obj = obj;
      this.isEnable = isEnable;
    }

    public JItemDisable(Object obj) {
      this(obj, true);
    }

    public boolean isEnabled() {
      return isEnable;
    }

    public void setEnabled(boolean isEnable) {
      this.isEnable = isEnable;
    }

    public String toString() {
      return obj.toString();
    }
    
    public class ComboListener implements ActionListener {
        JComboBox<Object> combo;

        Object currentItem;

        public ComboListener(JComboBox<Object> combo) {
          this.combo = combo;
          combo.setSelectedIndex(0);
          currentItem = combo.getSelectedItem();
        }

    	@Override
    	public void actionPerformed(ActionEvent e) {
              Object tempItem = combo.getSelectedItem();
              if (!((CanEnable) tempItem).isEnabled()) {
                combo.setSelectedItem(currentItem);
              } else {
                currentItem = tempItem;
              }
            }
    }

	interface CanEnable {

	  public void setEnabled(boolean isEnable);

	  public boolean isEnabled();

	}
}