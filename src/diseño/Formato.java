package dise�o;

import java.awt.Color;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Formato {

	public Formato() 
	{

	}
	
	public BigDecimal redondear(BigDecimal numero)
	{
		double n = numero.doubleValue();
		
		n=Math.rint(n*1E12)/1E12;
		
		return BigDecimal.valueOf(n);
	}
	
	//Formato a Decimales
	public String decimales(BigDecimal numero)
	{
		String decimal = null;
		double n = numero.doubleValue();
	
		if(n%1==0)
			decimal = String.format("%.0f", numero);
		else
			decimal = String.format("%.12f", numero);
		
		return decimal;
		
	}
	
	public String numeroL(BigDecimal numero)
	{
		DecimalFormat formateador = new DecimalFormat("###,###'###,###");
		
		return  formateador.format(numero);
	}
	
	public String noDecimales(String numero)
	{
		String n = "";
		
		for (int i = 0; i < numero.length(); i++) {
			n+=numero.charAt(i);
			if(numero.charAt(i)=='.')
			{
				for (int j = i+1; j < numero.length(); j++) {
					n+=numero.charAt(j);
					if(numero.charAt(j)>'0' && numero.charAt(j)<='9')
					{
						try {
							if(numero.charAt(j+1)=='0')
								break;
						} catch (Exception e) {
							// TODO: handle exception
							break;
						}
						
					}
				}
				break;
			}
		}
		return n;
	}
	
	//Centra texto en tabla
	public String centrar(String palabra)
	{
		String aux = "";
		
		for(int i = palabra.length()/2; i < palabra.length(); i++)
		{
			if(palabra.charAt(i)==' ')
			{
				aux = "<html> <P ALIGN=center>";
				for (int j = 0; j < i; j++) 
				{
					aux+=palabra.charAt(j);
				}
				aux+="<br>";
				for (int j = i; j < palabra.length(); j++) 
				{
					aux+=palabra.charAt(j);
				}
				aux+="</html>";
				break;
			}else
			{
				aux=palabra;
			}
		}
		
		return aux;
	}
	
	//Pone en mayuscula la primera letra y en minisculas las demas
	public String validarNombre(String palabra)
	{
		if(!palabra.equals(""))
		{
			String temp1 = "", temp2 = "";
			temp1=palabra.substring(0,1);
			temp1=temp1.toUpperCase();
			
			if(palabra.length()>1)
			{
				temp2=palabra.substring(1, palabra.length());
				temp2=temp2.toLowerCase();
			}
			
			return palabra=temp1+temp2;
		}
		return "";
	}
	
	private String[] delimitador(String linea, int pos)
	{
		int i = pos;
		String[] s = new String[2];
		s[0]="";
		
		while (linea.charAt(i)!='|') 
		{
			s[0]+=linea.charAt(i);
			i++;
		}
		s[1]=String.valueOf(i+1);
		
		return s;
	}
	
	public Color fondo()
	{ 
		Color c;
		int r, g, b;
		
		File archivo = null;
	    FileReader fr = null;
	    BufferedReader br = null;
	    String linea="";
	    
	    try {
	    	archivo = new File ("src/configuracion/conf.conf");
	        fr = new FileReader (archivo);
	        br = new BufferedReader(fr);
	        
	        linea=br.readLine();
	    }
	    catch(Exception e){
	         e.printStackTrace();
	    }finally{
	         try{                    
	            if(null != fr)  
	               fr.close();                       
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	    }
	    
	    String[] del = delimitador(linea, 0);
	    r = Integer.parseInt(del[0]);
	    del = delimitador(linea, Integer.parseInt(del[1]));
	    g = Integer.parseInt(del[0]);
	    del = delimitador(linea, Integer.parseInt(del[1]));
	    b = Integer.parseInt(del[0]);
	    
	    c = new Color(r, g, b);
	    
		return c;
	}
	
	public Color colorLetra()
	{ 
		Color c;
		int r, g, b;
		
		File archivo = null;
	    FileReader fr = null;
	    BufferedReader br = null;
	    String linea="";
	    
	    try {
	    	archivo = new File ("src/configuracion/conf.conf");
	        fr = new FileReader (archivo);
	        br = new BufferedReader(fr);
	        
	        for (int i = 0; i < 6; i++) {
	        	linea=br.readLine();
			}
	    }
	    catch(Exception e){
	         e.printStackTrace();
	    }finally{
	         try{                    
	            if(null != fr)  
	               fr.close();                       
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	    }
	    
	    String[] del = delimitador(linea, 0);
	    r = Integer.parseInt(del[0]);
	    del = delimitador(linea, Integer.parseInt(del[1]));
	    g = Integer.parseInt(del[0]);
	    del = delimitador(linea, Integer.parseInt(del[1]));
	    b = Integer.parseInt(del[0]);
	    
	    c = new Color(r, g, b);
	    
		return c;
	}
	
	public String tema()
	{
		File archivo = null;
	    FileReader fr = null;
	    BufferedReader br = null;
	    String linea="";
	    
	    try {
	    	archivo = new File ("src/configuracion/conf.conf");
	        fr = new FileReader (archivo);
	        br = new BufferedReader(fr);
	        
	        for (int i = 0; i < 2; i++) {
	        	linea=br.readLine();
			}
	        
	    }
	    catch(Exception e){
	         e.printStackTrace();
	    }finally{
	         try{                    
	            if(null != fr)  
	               fr.close();                       
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	    }
		
	    String[] del = delimitador(linea, 0);

		return del[0];
	}
	
	public Font titulo()
	{
		Font f;
		String arg0;
		int arg1, arg2;
		
		File archivo = null;
	    FileReader fr = null;
	    BufferedReader br = null;
	    String linea="";
	    
	    try {
	    	archivo = new File ("src/configuracion/conf.conf");
	        fr = new FileReader (archivo);
	        br = new BufferedReader(fr);
	        
	        for (int i = 0; i < 3; i++) {
	        	linea=br.readLine();
			}
	    }
	    catch(Exception e){
	         e.printStackTrace();
	    }finally{
	         try{                    
	            if(null != fr)  
	               fr.close();                       
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	    }
	    
	    String[] del = delimitador(linea, 0);
	    arg0 = del[0];
	    del = delimitador(linea, Integer.parseInt(del[1]));
	    arg1 = Integer.parseInt(del[0]);
	    del = delimitador(linea, Integer.parseInt(del[1]));
	    arg2 = Integer.parseInt(del[0]);
	    
	    f = new Font(arg0, arg1, arg2);
		
		return f;
	}
	
	public Font subtitulo()
	{
		Font f;
		String arg0;
		int arg1, arg2;
		
		File archivo = null;
	    FileReader fr = null;
	    BufferedReader br = null;
	    String linea="";
	    
	    try {
	    	archivo = new File ("src/configuracion/conf.conf");
	        fr = new FileReader (archivo);
	        br = new BufferedReader(fr);
	        
	        for (int i = 0; i < 4; i++) {
	        	linea=br.readLine();
			}
	    }
	    catch(Exception e){
	         e.printStackTrace();
	    }finally{
	         try{                    
	            if(null != fr)  
	               fr.close();                       
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	    }
	    
	    String[] del = delimitador(linea, 0);
	    arg0 = del[0];
	    del = delimitador(linea, Integer.parseInt(del[1]));
	    arg1 = Integer.parseInt(del[0]);
	    del = delimitador(linea, Integer.parseInt(del[1]));
	    arg2 = Integer.parseInt(del[0]);
	    
	    f = new Font(arg0, arg1, arg2);
		
		return f;
	}
	
	public Font texto()
	{
		Font f;
		String arg0;
		int arg1, arg2;
		
		File archivo = null;
	    FileReader fr = null;
	    BufferedReader br = null;
	    String linea="";
	    
	    try {
	    	archivo = new File ("src/configuracion/conf.conf");
	        fr = new FileReader (archivo);
	        br = new BufferedReader(fr);
	        
	        for (int i = 0; i < 5; i++) {
	        	linea=br.readLine();
			}
	    }
	    catch(Exception e){
	         e.printStackTrace();
	    }finally{
	         try{                    
	            if(null != fr)  
	               fr.close();                       
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	    }
	    
	    String[] del = delimitador(linea, 0);
	    arg0 = del[0];
	    del = delimitador(linea, Integer.parseInt(del[1]));
	    arg1 = Integer.parseInt(del[0]);
	    del = delimitador(linea, Integer.parseInt(del[1]));
	    arg2 = Integer.parseInt(del[0]);
	    
	    f = new Font(arg0, arg1, arg2);
		
		return f;
	}
}
