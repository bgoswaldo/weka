package dise�o;

import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JComboBox;
import javax.swing.text.JTextComponent;

public class JAutoComboBox extends JComboBox<Object>{

	private static final long serialVersionUID = 1L;
	
	private final Searchable<String,String> searchable;
	
	public boolean presionado = false;
	public boolean letra = false;
	public boolean numero = false;
	public boolean especiales = false;

	public JAutoComboBox(Searchable<String,String> s){

		super();
		this.searchable = s;
		setEditable(true);
		
		Component c = getEditor().getEditorComponent();
		
		if (c instanceof JTextComponent)
		{
			final JTextComponent txtCombo = (JTextComponent)c;
			txtCombo.addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent e) {
					// TODO Auto-generated method stub
					char car = e.getKeyChar();
					 
					if(especiales == true)
					{
						if (!(Character.isLetter(car)) && !(Character.isDigit(car)))
							e.consume();
					}else if(numero == true)
					{
						if ((Character.isDigit(car)))
							e.consume();
					}
					else if(letra == true)
					{
						if ((Character.isLetter(car)))
							e.consume();
					}
				}
				
				@Override
				public void keyReleased(KeyEvent e) {
					// TODO Auto-generated method stub
					presionado=true;
					if(e.getKeyCode() !=  KeyEvent.VK_DOWN && e.getKeyCode() !=  KeyEvent.VK_UP 
							&& e.getKeyCode() !=  KeyEvent.VK_LEFT && e.getKeyCode() !=  KeyEvent.VK_RIGHT)
						update();
				}
				
				@Override
				public void keyPressed(KeyEvent e) {
					// TODO Auto-generated method stub
					if(e.getKeyCode() == KeyEvent.VK_ENTER)
					{
						setPopupVisible(false);
						KeyboardFocusManager foco = KeyboardFocusManager.getCurrentKeyboardFocusManager();
						foco.focusNextComponent();
						presionado=true;
					}
				}
				
				public void update(){
					List<String> item = new ArrayList<String>(searchable.search(new Formato().validarNombre(txtCombo.getText())));
					Set<String> itemSet = new HashSet<String>();

					for (String s : item){
						itemSet.add(s.toLowerCase());
					}

					Collections.sort(item);
					setEditable(false);
					removeAllItems();

					if (!itemSet.contains(new Formato().validarNombre(txtCombo.getText().toLowerCase()))){
						addItem(txtCombo.getText());
					}

					for (String s : item) {
						addItem(s);
					}

					setEditable(true);
					setPopupVisible(true);
					requestFocus();
				}
			});
			txtCombo.addFocusListener(new FocusListener(){
				@Override
				public void focusGained(FocusEvent arg0) {
					if (txtCombo.getText().length() > 0){
						setPopupVisible(true);
					}
				}

				@Override
				public void focusLost(FocusEvent arg0) {						

				}
			});
		}
		else
		{
			throw new IllegalStateException("Error al cargar el componente");
		}
	}
}
