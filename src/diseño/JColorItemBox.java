package dise�o;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class JColorItemBox extends JButton implements ListCellRenderer<Object>   {
 
	private static final long serialVersionUID = 1L;
	int itemDisable[];

	public JColorItemBox(int itemDisable[]) {
        setOpaque(false);
        this.itemDisable=itemDisable;
    }


	 @Override
	    public Component getListCellRendererComponent(JList<?> list, Object value,
	            int index, boolean isSelected, boolean cellHasFocus) {
		
		 
		 
		 for (int i = 0; i < itemDisable.length; i++) {
				 if (index==itemDisable[i]) {
			        	setBackground(Color.RED);
			        	setForeground(Color.BLACK);
					}
			}
			 if (isSelected && getBackground()!=Color.RED) {
		            setBackground(Color.BLUE);
		            setForeground(Color.YELLOW);
		        }else
		        {
		        	setForeground(Color.BLACK);
			        setBackground(Color.LIGHT_GRAY);
		        }
			
	    	setText(value.toString());
	        return this;
	   }
}
