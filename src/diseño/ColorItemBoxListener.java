package dise�o;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

public class ColorItemBoxListener implements ActionListener {
    JComboBox<String> combo;
    int itemDisable[];
    Object currentItem;

    public ColorItemBoxListener(JComboBox<String> combo, int itemDisable[]) {
      this.combo = combo;
      currentItem = combo.getSelectedItem();
      this.itemDisable=itemDisable;
      combo.setFont(new Formato().subtitulo());
      combo.setForeground(new Formato().colorLetra());
    }

	@Override
	public void actionPerformed(ActionEvent e) {
          
          for (int i = 0; i < itemDisable.length; i++) {
        	  if(itemDisable[i]==combo.getSelectedIndex())
        	  {
        		  combo.setSelectedIndex(itemDisable[i]+1);
        		  break;
        	  }
		}
	}
}
