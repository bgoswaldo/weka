package dise�o;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AutoComboBoxListener implements Searchable<String,String>{
	
	private List<String> item = new ArrayList<String>();
	
	public AutoComboBoxListener(List<String> terms){
		this.item.addAll(terms);
	}

	@Override
	public Collection<String> search(String value) {

		List<String> encontrado = new ArrayList<String>();

		for (String s : item)
		{
			if (s.indexOf(value) == 0){
				encontrado.add(s);
			}
		}

		return encontrado;
	}
}
