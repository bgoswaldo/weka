package dise�o;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Random;

public class JColor extends javax.swing.JPanel{

	private static final long serialVersionUID = 1L;
	
	private Color color1 = new Color(0,0,0);
    private Color color2 = new Color(0,0,0);
      
    public JColor(Color c1, Color c2){
        color1 = c1;
        color2 = c2;        
    }
    
    @Override
    public void paint(Graphics g) {    
           	 
    	  Graphics2D g2d=(Graphics2D)g;
    	  Rectangle clip = g2d.getClipBounds();
    	  g2d.setPaint(new GradientPaint(0.0f, 0.0f, color1.brighter(), 0.0f ,getWidth(), color2, true));        
    	  g2d.fillRect(clip.x, clip.y, clip.width, clip.height);  
    	  setOpaque(false);
    	  super.paintComponents(g);
    }
    
    public void setRandomColor(){
        Random rand = new Random();
        this.color1 = new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255));
        this.color2 = new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255));
    }    
}

class JPaneColor extends javax.swing.JPanel{

	private static final long serialVersionUID = 1L;
	
	private Color color1 = new Color(0,0,0);
    private Color color2 = new Color(0,0,0);
      
    public JPaneColor(Color c1, Color c2){
        color1 = c1;
        color2 = c2;        
    }
    
    @Override
    public void paint(Graphics g) {    
           	 
    	  Graphics2D g2d=(Graphics2D)g;
	   	  Rectangle clip = g2d.getClipBounds();
	   	  g2d.setPaint(new GradientPaint(getHeight(),  0.0f , color1.brighter(), 0.0f ,getWidth(), color2.brighter(), true));        
	   	  g2d.fillRect(clip.x, clip.y, clip.width, clip.height);    
	   	  setOpaque(false);
	   	  super.paintComponents(g);
    }
    
    public void setRandomColor(){
        Random rand = new Random();
        this.color1 = new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255));
        this.color2 = new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255));
    }    
}
