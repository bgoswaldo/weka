package gui.ventana;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;

import dise�o.Formato;
import gui.panel.pnlArchivo;
import modelo.archivo;
import modelo.expresiones;
import modelo.leerArchivo;

public class ventanaPrincipal extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;

	JMenuBar menuBar = new JMenuBar();
	
	JMenu mnArchivo = new JMenu("Archivo");
	JMenuItem mntmLeer = new JMenuItem("Abrir");
	JMenuItem mntmGuardar = new JMenuItem("Guardar");
	
	JMenu mnProcesamiento = new JMenu("Procesamiento");
	JMenuItem mntmTFrecuencia = new JMenuItem("Tabla de Frecuencia");
	JMenuItem mntmVNNominales = new JMenuItem("Valores no nominales");
	JMenuItem mntmValidacionesExpReg = new JMenuItem("Validaciones de RegExp");
	JMenuItem mntmTransformacionDatos = new JMenuItem("Transformacion de Datos");
	JMenuItem mntmCorrelacion = new JMenuItem("Correlacion");
	
	JMenu mnAlgoritmo = new JMenu("Algoritmos");
	JMenuItem mntmZeroR = new JMenuItem("ZeroR");
	JMenuItem mntmOneR = new JMenuItem("OneR");
	JMenuItem mntmNaiveBayes = new JMenuItem("Na�ve Bayes");
	
	pnlArchivo pnlArchivo;
	
	String url;
	File fichero;
	leerArchivo archivo;
	
	public ventanaPrincipal() 
	{
		setTitle("Proyecto");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(720, 400);
		setMinimumSize(new Dimension(640, 480));
		
		iniPane();
		
		setJMenuBar(menuBar);
		
		menuBar.add(mnArchivo);
		mnArchivo.add(mntmLeer);
		mntmLeer.addActionListener(this);
		mnArchivo.add(mntmGuardar);
		mntmGuardar.setEnabled(false);
		mntmGuardar.addActionListener(this);
		
		menuBar.add(mnProcesamiento);
		
		mntmTFrecuencia.setEnabled(false);
		mnProcesamiento.add(mntmTFrecuencia);
		mntmTFrecuencia.addActionListener(this);
		mntmVNNominales.setEnabled(false);
		mnProcesamiento.add(mntmVNNominales);
		mntmVNNominales.addActionListener(this);
		mntmValidacionesExpReg.setEnabled(false);
		mnProcesamiento.add(mntmValidacionesExpReg);
		mntmValidacionesExpReg.addActionListener(this);
		mntmTransformacionDatos.setEnabled(false);
		mnProcesamiento.add(mntmTransformacionDatos);
		mntmTransformacionDatos.addActionListener(this);
		mntmCorrelacion.setEnabled(false);
		mnProcesamiento.add(mntmCorrelacion);
		mntmCorrelacion.addActionListener(this);
		
		menuBar.add(mnAlgoritmo);
		mnAlgoritmo.add(mntmZeroR);
		mntmZeroR.setEnabled(false);
		mntmZeroR.addActionListener(this);
		mnAlgoritmo.add(mntmOneR);
		mntmOneR.setEnabled(false);
		mntmOneR.addActionListener(this);
		mnAlgoritmo.add(mntmNaiveBayes);
		mntmNaiveBayes.setEnabled(false);
		mntmNaiveBayes.addActionListener(this);
	}
	
	public void iniPane()
	{
		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);

		SubstanceLookAndFeel.setSkin(new Formato().tema());
		SwingUtilities.updateComponentTreeUI(ventanaPrincipal.this);
		
		pnlArchivo = new pnlArchivo(getContentPane().getBounds());
		setContentPane(pnlArchivo);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		Object presionado = arg0.getSource();
		
		if(presionado == mntmLeer)
		{
			mntmGuardar.setEnabled(true);
			mntmTFrecuencia.setEnabled(true);
			mntmVNNominales.setEnabled(true);
			mntmValidacionesExpReg.setEnabled(true);
			mntmCorrelacion.setEnabled(true);
			mntmNaiveBayes.setEnabled(true);
			mntmOneR.setEnabled(true);
			mntmTransformacionDatos.setEnabled(true);
			mntmZeroR.setEnabled(true);
			
			JFileChooser file = new JFileChooser();
			file.setFileFilter(new FileNameExtensionFilter("Archivo csv","csv"));
			
			int result = file.showOpenDialog(getContentPane());
			if (result == JFileChooser.APPROVE_OPTION)
			{
				fichero = file.getSelectedFile();
				
				archivo = new leerArchivo();
				url = fichero.getAbsolutePath();
				pnlArchivo.actualizarTable(archivo.encabezado(url), archivo.mostrar(url), archivo.getRow(url));
				pnlArchivo.setUrl(url);
				pnlArchivo.setNombre(file.getSelectedFile().getName());
				pnlArchivo.menu(0);
				boolean existe = new expresiones().existe(file.getSelectedFile().getName());
				if(existe == false)
				{
					new expresiones().crearArchivo(pnlArchivo.getColumnas().length,file.getSelectedFile().getName());
				}
			}
		}
		else if(presionado == mntmGuardar)
		{			
			String path = "/";  
			URL ruta = this.getClass().getResource(path);
			
			JFileChooser file = new JFileChooser();
			file.setFileFilter(new FileNameExtensionFilter("Archivo csv","csv"));
			File guardar;
			File fichero = new File(ruta.getPath()+"temp/temp.csv");
			
			int result = file.showSaveDialog(getContentPane());
			if (result == JFileChooser.APPROVE_OPTION)
			{
				guardar = new File(file.getSelectedFile().getAbsoluteFile().toString()+".csv");
				fichero.renameTo(guardar);
				pnlArchivo.setUrl(guardar.getAbsolutePath());
				pnlArchivo.guardar();
				
				new expresiones().guardarExp(pnlArchivo.getNombre(),guardar.getName());
			}
			JOptionPane.showMessageDialog(null, "Se guardo con exito", "Guardado", JOptionPane.ERROR_MESSAGE);
		}
		else if(presionado == mntmTFrecuencia)
		{
			if(pnlArchivo.modificado() == false)
			{
				ArrayList<archivo> list = archivo.valores(fichero.getAbsolutePath(), 0);
				list.remove(0);
				pnlArchivo.actualizarTable(list);
			}else
			{
				String path = "/temp";  
				URL ruta = this.getClass().getResource(path);
				
				ArrayList<archivo> list = archivo.valores(ruta.getPath()+"/temp.csv", 0);
				list.remove(0);
				pnlArchivo.actualizarTable(list);
			}
		}else if(presionado == mntmVNNominales)
		{
			pnlArchivo.showTableForNotNominalValues();
		}
		else if(presionado == mntmValidacionesExpReg)
		{
			pnlArchivo.validateRegExp();
		}
		else if(presionado == mntmTransformacionDatos)
		{
			pnlArchivo.transformacion();
		}
	}
}