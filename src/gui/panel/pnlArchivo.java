package gui.panel;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.GroupLayout.Alignment;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import dise�o.Formato;
import dise�o.JColor;
import modelo.archivo;
import modelo.estadistica;
import modelo.expresiones;
import modelo.leerArchivo;
import modelo.tipoDato;
import modelo.transformaciones;

public class pnlArchivo extends JScrollPane implements ActionListener, ItemListener{

	private static final long serialVersionUID = 1L;
	
	JColor contentPane;
	JButton btnEliminarC = new JButton("Eliminar Columna");
	JButton btnEliminarF = new JButton("Eliminar Fila");
	JButton btnRestaurar = new JButton("Restaurar");
	
	JLabel lblFrecuencia = new JLabel("Atributo:");
	JComboBox<String> cmbFrecuencia;
	
	JScrollPane scrollClase;
	JTable tblClase;
	DefaultTableModel dtmClase;
	String aClase[][] = new String [0][0];
	
	JLabel lblTransformacion;
	JLabel lblTTransformacion = new JLabel("Transformacion de Datos");
	JTextField txtTransformacion;
	JButton btnTransformacionA;
	JButton btnTransformacionC;
	JDialog dialogo;
	
	JScrollPane scrollTransformacion;
	JTable tblTransformacion;
	DefaultTableModel dtmTransformacion;
	String aTransformacion[][] = new String [0][0];
	
	JScrollPane scrollFrecuencia;
	JTable tblFrecuencia;
	DefaultTableModel dtmFrecuencia;
	String aFrecuencia[][] = new String [0][0];
	
	JLabel lblNNominales = new JLabel("Datos de valores No Nominales");
	
	JScrollPane scrollNNominales;
	JTable tblNNominales;
	DefaultTableModel dtmNNominales;
	String aNNominales[][] = new String[0][0];
	
	JLabel lblValidacionesClase = new JLabel("Atributo:");
	JComboBox<String> cmbValidacionesClase;
	JLabel lblValidacionesExpReg = new JLabel("Expresion Regular:");
	JButton btnAplicar = new JButton("Aplicar");
	JTextField txtValidacionesExpReg;
	
	JScrollPane scrollValidacionesExpReg;
	JTable tblValidacionesExpReg;
	DefaultTableModel dtmValidacionesExpReg;
	String aValidacionesExpReg[][] = new String[0][0];
	
	leerArchivo archivo = new leerArchivo();
	tipoDato dato = new tipoDato();
	estadistica estadistica = new estadistica();
	
	Color fondo = new Formato().fondo();
	Color colorLetra = new Formato().colorLetra();
	Font titulo = new Formato().titulo();
	Font subtitulo = new Formato().subtitulo();
	Font texto = new Formato().texto();
	
	String url;
	String nombre;
	String fila[];
	int cont;
	int eliminado;
	boolean mod = false;
	boolean modC = false;
	boolean pres = false;
	
	private static final int RESTAURAR 		= 0;
	private static final int FRECUENCIA 	= 1;
	private static final int NNOMINALES 	= 2;
	private static final int EXPRESIONES 	= 3;
	private static final int TRANSFORMACION = 4;
	
	public pnlArchivo() 
	{
		
	}
	
	public pnlArchivo(Rectangle dim) 
	{
		setBounds(dim);
		contentPane = new JColor(Color.DARK_GRAY, Color.WHITE);
		setViewportView(contentPane);
		
		btnEliminarC.setForeground(colorLetra);
		btnEliminarC.setFont(subtitulo);
		btnEliminarC.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnEliminarC.setVisible(false);
		btnEliminarC.addActionListener(this);
		
		btnEliminarF.setForeground(colorLetra);
		btnEliminarF.setFont(subtitulo);
		btnEliminarF.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnEliminarF.setVisible(false);
		btnEliminarF.addActionListener(this);
		
		btnRestaurar.setForeground(colorLetra);
		btnRestaurar.setFont(subtitulo);
		btnRestaurar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnRestaurar.setVisible(false);
		btnRestaurar.addActionListener(this);
		
		lblFrecuencia.setForeground(colorLetra);
		lblFrecuencia.setFont(subtitulo);
		lblFrecuencia.setVisible(false);
		
		cmbFrecuencia = new JComboBox<String>();
		cmbFrecuencia.addItemListener(this);
		cmbFrecuencia.setVisible(false);
		
		String[] cClase = {};
		
		dtmClase = new DefaultTableModel(aClase, cClase);
		
		tblClase = new JTable(dtmClase);
		tblClase.setRowHeight(40);
		tblClase.setCellSelectionEnabled(true);
		tblClase.setFont(texto);
		tblClase.setForeground(colorLetra);
		
		JTableHeader thClase = tblClase.getTableHeader();  
		thClase.setFont(texto); 
		thClase.setForeground(colorLetra);
		
		scrollClase = new JScrollPane();
		scrollClase.getViewport().setOpaque(false);
		scrollClase.setOpaque(false);
		scrollClase.setBorder(BorderFactory.createEmptyBorder());
		scrollClase.setViewportView(tblClase);
		
		lblTTransformacion.setForeground(colorLetra);
		lblTTransformacion.setFont(subtitulo);
		lblTTransformacion.setVisible(false);
		
		scrollTransformacion = new JScrollPane();
		scrollTransformacion.getViewport().setOpaque(false);
		scrollTransformacion.setOpaque(false);
		scrollTransformacion.setBorder(BorderFactory.createEmptyBorder());
		
		String[] cFrecuencia = {"Nombre","Frecuencia"};
		
		dtmFrecuencia = new DefaultTableModel(aFrecuencia, cFrecuencia);
		
		tblFrecuencia = new JTable(dtmFrecuencia);
		tblFrecuencia.setRowHeight(40);
		tblFrecuencia.setCellSelectionEnabled(true);
		tblFrecuencia.setFont(texto);
		tblFrecuencia.setForeground(colorLetra);
		
		JTableHeader thFrecuencia = tblFrecuencia.getTableHeader();  
		thFrecuencia.setFont(texto); 
		thFrecuencia.setForeground(colorLetra);
		
		scrollFrecuencia = new JScrollPane();
		scrollFrecuencia.getViewport().setOpaque(false);
		scrollFrecuencia.setOpaque(false);
		scrollFrecuencia.setVisible(false);
		scrollFrecuencia.setBorder(BorderFactory.createEmptyBorder());
		scrollFrecuencia.setViewportView(tblFrecuencia);
		
		lblNNominales.setForeground(colorLetra);
		lblNNominales.setFont(subtitulo);
		lblNNominales.setVisible(false);
		
		String[] cNNominales = {"Atributo","Media","Mediana","Moda", 
				"Varianza","Desv.Estandar","Min","Max"};
		
		dtmNNominales = new DefaultTableModel(aNNominales, cNNominales);
		
		tblNNominales = new JTable(dtmNNominales);
		tblNNominales.setRowHeight(40);
		tblNNominales.setCellSelectionEnabled(true);
		tblNNominales.setFont(texto);
		tblNNominales.setForeground(colorLetra);
		
		JTableHeader thNNominales = tblNNominales.getTableHeader();  
		thNNominales.setFont(texto); 
		thNNominales.setForeground(colorLetra);
		
		scrollNNominales = new JScrollPane();
		scrollNNominales.getViewport().setOpaque(false);
		scrollNNominales.setOpaque(false);
		scrollNNominales.setVisible(false);
		scrollNNominales.setBorder(BorderFactory.createEmptyBorder());
		scrollNNominales.setViewportView(tblNNominales);
		
		lblValidacionesClase.setForeground(colorLetra);
		lblValidacionesClase.setFont(subtitulo);
		lblValidacionesClase.setVisible(false);
		
		cmbValidacionesClase = new JComboBox<String>();
		cmbValidacionesClase.addItemListener(this);
		cmbValidacionesClase.setVisible(false);
		
		lblValidacionesExpReg.setForeground(colorLetra);
		lblValidacionesExpReg.setFont(subtitulo);
		lblValidacionesExpReg.setVisible(false);
		
		btnAplicar.setForeground(colorLetra);
		btnAplicar.setFont(subtitulo);
		btnAplicar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnAplicar.setVisible(false);
		btnAplicar.addActionListener(this);
		
		txtValidacionesExpReg = new JTextField();
		txtValidacionesExpReg.setVisible(false);
		
		String[] cValidacionesExpReg = 
			{	"Clase",
				"No. ValoresInvalidos",
				"Expresion regular",
				"Posiciones de Valores Invalidos",
				"Valores Invalidos"};
		
		dtmValidacionesExpReg = new DefaultTableModel(aValidacionesExpReg, cValidacionesExpReg);
		
		tblValidacionesExpReg = new JTable(dtmValidacionesExpReg);
		tblValidacionesExpReg.setRowHeight(40);
		tblValidacionesExpReg.setCellSelectionEnabled(true);
		tblValidacionesExpReg.setFont(texto);
		tblValidacionesExpReg.setForeground(colorLetra);
		
		JTableHeader thValidacionesExpReg = tblValidacionesExpReg.getTableHeader();  
		thValidacionesExpReg.setFont(texto); 
		thValidacionesExpReg.setForeground(colorLetra);
		
		scrollValidacionesExpReg = new JScrollPane();
		scrollValidacionesExpReg.getViewport().setOpaque(false);
		scrollValidacionesExpReg.setOpaque(false);
		scrollValidacionesExpReg.setVisible(false);
		scrollValidacionesExpReg.setBorder(BorderFactory.createEmptyBorder());
		scrollValidacionesExpReg.setViewportView(tblValidacionesExpReg);
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		
		gl_contentPane.setHorizontalGroup(
		gl_contentPane.createParallelGroup(Alignment.LEADING)
		.addGroup(gl_contentPane.createSequentialGroup()
				.addGap(111,111,468)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addGap(6)
						.addComponent(btnEliminarC)
						.addGap(6)
						.addComponent(btnEliminarF)
						.addGap(6)
						.addComponent(btnRestaurar)
						)
				.addGap(111,111,189))
		.addGroup(gl_contentPane.createSequentialGroup()
				.addGap(6)
				.addComponent(scrollClase,GroupLayout.PREFERRED_SIZE,600,Short.MAX_VALUE)
				.addGap(6))
		.addGroup(gl_contentPane.createSequentialGroup()
				.addGroup(gl_contentPane.createSequentialGroup()
						.addGap(111,111,222)
						.addComponent(lblTTransformacion)
						.addGap(18,18,222)))
		.addGroup(gl_contentPane.createSequentialGroup()
				.addGap(6)
				.addComponent(scrollTransformacion,GroupLayout.PREFERRED_SIZE,600,Short.MAX_VALUE)
				.addGap(6))
		.addGroup(gl_contentPane.createSequentialGroup()
				.addGroup(gl_contentPane.createSequentialGroup()
						.addGap(18,18,189)
						.addComponent(lblFrecuencia)
						.addGap(6)
						.addComponent(cmbFrecuencia)
						.addGap(18,18,189)))
		.addGroup(gl_contentPane.createSequentialGroup()
				.addGap(6)
				.addComponent(scrollFrecuencia,GroupLayout.PREFERRED_SIZE,600,Short.MAX_VALUE)
				.addGap(6))
		.addGroup(gl_contentPane.createSequentialGroup()
				.addGroup(gl_contentPane.createSequentialGroup()
						.addGap(111,111,222)
						.addComponent(lblNNominales)
						.addGap(18,18,222)))
		.addGroup(gl_contentPane.createSequentialGroup()
				.addGap(6)
				.addComponent(scrollNNominales,GroupLayout.PREFERRED_SIZE,600,Short.MAX_VALUE)
				.addGap(6))
		.addGroup(gl_contentPane.createSequentialGroup()
				.addGroup(gl_contentPane.createSequentialGroup()
						.addGap(18,18,189)
						.addComponent(lblValidacionesClase)
						.addGap(6)
						.addComponent(cmbValidacionesClase)
						.addGap(6)
						.addComponent(lblValidacionesExpReg)
						.addGap(6)
						.addComponent(txtValidacionesExpReg)
						.addGap(6)
						.addComponent(btnAplicar)
						.addGap(18,18,189)))
		.addGroup(gl_contentPane.createSequentialGroup()
				.addGap(6)
				.addComponent(scrollValidacionesExpReg,GroupLayout.PREFERRED_SIZE,600,Short.MAX_VALUE)
				.addGap(6)));
		
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createSequentialGroup()
				.addGap(6)
				.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGap(3)
						.addComponent(btnEliminarC, 26, 26, 26)
						.addComponent(btnEliminarF, 26, 26, 26)
						.addComponent(btnRestaurar, 26, 26, 26)
						)
				.addGap(6)
				.addComponent(scrollClase,GroupLayout.DEFAULT_SIZE,148,Short.MAX_VALUE)
				.addGap(6)
				.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGap(3)
						.addComponent(lblTTransformacion))
				.addGap(6)
				.addComponent(scrollTransformacion,GroupLayout.DEFAULT_SIZE,148,Short.MAX_VALUE)
				.addGap(6)
				.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGap(3)
						.addComponent(lblFrecuencia)
						.addComponent(cmbFrecuencia, 26, 26, 26))
				.addGap(6)
				.addComponent(scrollFrecuencia,GroupLayout.DEFAULT_SIZE,148,Short.MAX_VALUE)
				.addGap(6)
				.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNNominales))
				.addGap(6)
				.addComponent(scrollNNominales,GroupLayout.DEFAULT_SIZE,88,Short.MAX_VALUE)
				.addGap(6)
				.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGap(3)
						.addComponent(lblValidacionesClase)
						.addComponent(cmbValidacionesClase, 26, 26, 26)
						.addComponent(lblValidacionesExpReg)
						.addComponent(txtValidacionesExpReg, 26, 26, 26)
						.addComponent(btnAplicar, 26, 26, 26))
				.addGap(6)
				.addComponent(scrollValidacionesExpReg,GroupLayout.DEFAULT_SIZE,148,Short.MAX_VALUE)
				.addGap(6)
				);
		contentPane.setLayout(gl_contentPane);
	}
	
	public void menu(int visible)
	{
		switch (visible) {
		case RESTAURAR:
			scrollFrecuencia.setVisible(false);
			lblFrecuencia.setVisible(false);
			cmbFrecuencia.setVisible(false);
			lblNNominales.setVisible(false);
			scrollNNominales.setVisible(false);
			
			lblValidacionesClase.setVisible(false);
			cmbValidacionesClase.setVisible(false);
			lblValidacionesExpReg.setVisible(false);
			txtValidacionesExpReg.setVisible(false);
			scrollValidacionesExpReg.setVisible(false);
			btnAplicar.setVisible(false);
			
			scrollTransformacion.setVisible(false);
			lblTTransformacion.setVisible(false);
			break;
		case FRECUENCIA:
			cmbFrecuencia.setVisible(true);
			lblFrecuencia.setVisible(true);
			scrollFrecuencia.setVisible(true);
			break;
		case NNOMINALES:
			lblNNominales.setVisible(true);
			scrollNNominales.setVisible(true);
			break;
		case EXPRESIONES:
			lblValidacionesClase.setVisible(true);
			cmbValidacionesClase.setVisible(true);
			lblValidacionesExpReg.setVisible(true);
			txtValidacionesExpReg.setVisible(true);
			scrollValidacionesExpReg.setVisible(true);
			btnAplicar.setVisible(true);
			break;
		case TRANSFORMACION:
			scrollTransformacion.setVisible(true);
			lblTTransformacion.setVisible(true);
			break;
		default:
			break;
		}
	}
	
	public void setUrl(String url)
	{
		this.url = url;
	}
	
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	
	public String getNombre()
	{
		return nombre;
	}
	
	public void guardar()
	{
		btnRestaurar.setVisible(false);
		mod = false;
		modC = false;
	}
	
	public void actualizarTable(String clases[], String datos[], int row)
	{	
		btnEliminarC.setVisible(true);
		btnEliminarF.setVisible(true);
		
		dtmClase.setColumnIdentifiers(clases);
		dtmClase.setRowCount(row);
		
		int j = 0;
		for (int i = 0; i < datos.length; i++) 
		{
			for (int k = 0; k < dtmClase.getColumnCount(); k++) 
			{
				dtmClase.setValueAt(datos[i], j, k);
				i++;
			}
			i--;
			j++;
		}
		
		DefaultTableCellRenderer tcrClase = new DefaultTableCellRenderer();
		
		for (int i = 0; i < clases.length; i++) 
		{
			tcrClase.setHorizontalAlignment(SwingConstants.CENTER);
			tblClase.getColumnModel().getColumn(i).setCellRenderer(tcrClase);
		}
	}
	
	public void actualizarTable(ArrayList<archivo> list)
	{
		String columnas[] = getColumnas();
		
		menu(1);
		
		cmbFrecuencia.removeAllItems();
		for (int i = 0; i < columnas.length; i++) 
		{
			cmbFrecuencia.addItem(columnas[i]);
		}
		
		int cont = dtmFrecuencia.getRowCount();
		
		for (int i = 0; i < cont; i++)
		{
			dtmFrecuencia.removeRow(0);
		}
		
		for (archivo ar : list)
		{
			Object[] row={ar.nombre,ar.repeticion}; 
			
			dtmFrecuencia.addRow(row);
		}
	}
	
	public boolean modificado()
	{
		return mod;
	}
	
	public String[] getColumnas()
	{
		String[] columna = new String[tblClase.getColumnCount()];
		
		for (int i = 0; i < columna.length; i++) 
		{
			columna[i] = tblClase.getColumnName(i);
		}
		
		return columna;
	}
	
	public String[] getFila()
	{
		String[] fila = new String[dtmClase.getRowCount()*dtmClase.getColumnCount()];
		int j = 0;

		for (int i = 0; i < fila.length; i++) 
		{
			for (int k = 0; k < dtmClase.getColumnCount(); k++) 
			{
				fila[i] = (String)dtmClase.getValueAt(j, k);
				i++;
			}
			i--;
			j++;
		}
		return fila;
	}
	
	public String[] getFilas()
	{
		String[] fila = new String[(dtmClase.getRowCount()*dtmClase.getColumnCount())-dtmClase.getRowCount()];
		int j = 0;

		for (int i = 0; i < fila.length; i++) 
		{
			for (int k = 0; k < dtmClase.getColumnCount(); k++) 
			{
				if(k != eliminado)
				{
					fila[i] = (String)dtmClase.getValueAt(j, k);
					i++;
				}
			}
			i--;
			j++;
		}
		return fila;
	}
	
	private String[] getFilasByColumn(int columnNumber)
	{
		String[] fila = new String[(dtmClase.getRowCount())];
		int j = columnNumber;
		for (int i = 0; i < fila.length; i++) 
		{
			fila[i] = (String)dtmClase.getValueAt(i, j);
		}
		return fila;
	}
	
	public void showTableForNotNominalValues()
	{
		menu(2);
		
		String columna[] = getColumnas();
		String columnaNoNominal[] = new String[columna.length];
		
		int numColumnaNoNominal[] = new int[columna.length];
		int totalColNoNominales = 0;
		String fila[];
		int cont = dtmNNominales.getRowCount();
		
		for (int i = 0; i < cont; i++)
		{
			dtmNNominales.removeRow(0);
		}
		
		for (int i = 0; i < columna.length; i++) 
		{
			fila = getFilasByColumn(i);
			if(dato.isNotNominal(i,fila))
			{
				columnaNoNominal[totalColNoNominales] = columna[i]; 
				numColumnaNoNominal[totalColNoNominales] = i;
				totalColNoNominales++;
			}
		}
		
		String[][] resultDeColumnaNoNominal = new String[totalColNoNominales][]; 

		for (int i = 0; i < totalColNoNominales; i++) {
			resultDeColumnaNoNominal[i] = estadistica.applyOperations(getFilasByColumn(numColumnaNoNominal[i]));
			resultDeColumnaNoNominal[i][estadistica.ATTR] = columnaNoNominal[i];
		}
		
		for (int i = 0; i < totalColNoNominales; i++) 
		{
			dtmNNominales.addRow(new Object[]{
					resultDeColumnaNoNominal[i][estadistica.ATTR],
					resultDeColumnaNoNominal[i][estadistica.MEDIA],
					resultDeColumnaNoNominal[i][estadistica.MEDIANA],
					resultDeColumnaNoNominal[i][estadistica.MODA],
					resultDeColumnaNoNominal[i][estadistica.VARIANZA],
					resultDeColumnaNoNominal[i][estadistica.DESVESTANDAR],
					resultDeColumnaNoNominal[i][estadistica.MIN],
					resultDeColumnaNoNominal[i][estadistica.MAX]
					});
		}
	}
	
	public void transformacion()
	{
		String columna[] = getColumnas();
		
		String[] cTransformacion = {};
		
		dtmTransformacion = new DefaultTableModel(aTransformacion, cTransformacion);
		
		tblTransformacion = new JTable(dtmTransformacion);
		tblTransformacion.setRowHeight(40);
		tblTransformacion.setCellSelectionEnabled(true);
		tblTransformacion.setFont(texto);
		tblTransformacion.setForeground(colorLetra);
		
		JTableHeader thTransformacion = tblTransformacion.getTableHeader();  
		thTransformacion.setFont(texto); 
		thTransformacion.setForeground(colorLetra);
		
		scrollTransformacion.setViewportView(tblTransformacion);
		
		for (this.cont = 0; this.cont < columna.length; this.cont++) 
		{
			Object numeros[] = null;
			fila = getFilasByColumn(this.cont);
			if(dato.isNotNominal(this.cont,fila))
			{
				numeros = new transformaciones().numericos(fila);
				dtmTransformacion.addColumn(tblClase.getColumnName(this.cont),numeros);
			}
			else
			{
				dialogo = new JDialog(SwingUtilities.getWindowAncestor(contentPane), Dialog.ModalityType.APPLICATION_MODAL);
				
				lblTransformacion = new JLabel();
				lblTransformacion.setBounds(6, 6, 363, 28);
				lblTransformacion.setText("Dominio para el atributo: \""+tblClase.getColumnName(this.cont)+"\" (separado por \"-\")");
				lblTransformacion.setForeground(colorLetra);
				lblTransformacion.setFont(texto);
				dialogo.getContentPane().add(lblTransformacion);		
				
				btnTransformacionA = new JButton("Siguiente");
				btnTransformacionA.setBounds(181, 39, 101, 28); 
				btnTransformacionA.setForeground(colorLetra);
				btnTransformacionA.setFont(subtitulo);
				btnTransformacionA.setCursor(new Cursor(Cursor.HAND_CURSOR));
				btnTransformacionA.addActionListener(this);
				dialogo.getContentPane().add(btnTransformacionA);
				
				btnTransformacionC = new JButton("Cancelar");
				btnTransformacionC.setBounds(292, 39, 101, 28); 
				btnTransformacionC.setForeground(colorLetra);
				btnTransformacionC.setFont(subtitulo);
				btnTransformacionC.setCursor(new Cursor(Cursor.HAND_CURSOR));
				btnTransformacionC.addActionListener(this);
				dialogo.getContentPane().add(btnTransformacionC);
				
				txtTransformacion = new JTextField();
				txtTransformacion.setBounds(369, 9, 222, 28);
				dialogo.getContentPane().add(txtTransformacion);
				
				dialogo.setSize(606, 101);
				dialogo.getContentPane().setLayout(null);
				dialogo.setLocationRelativeTo(null);
				dialogo.setResizable(false);
				dialogo.setTitle("Transformacion");
			    dialogo.setVisible(true);
			}
		}
		menu(TRANSFORMACION);
	}
	
	public void validateRegExp()
	{	
		String clases[] = getColumnas();
		expresiones exp = new expresiones(clases.length,nombre);
		
		for (int i = 0; i < tblClase.getRowCount(); i++) 
		{
			//for (int j = 0; j < exp.getExpresiones(clases.length,nombre).length; j++)
			for (int j = 0; j < clases.length; j++)
			{
				exp.validateValue(j, i, dtmClase.getValueAt(i, j).toString(),clases.length);
			}
		}
		showTableForInvalidData(clases,exp);
	}
	
	public void showTableForInvalidData(String clases[],expresiones exp)
	{
		int cont = dtmValidacionesExpReg.getRowCount();
		for (int i = 0; i < cont; i++)
		{
			dtmValidacionesExpReg.removeRow(0);
		}
		
		for (int i = 0; i < clases.length; i++) 
		{
			dtmValidacionesExpReg.addRow(new Object[]{
					clases[i], exp.getCantidadValoresInvalidos()[i], exp.getExpresionesRegulares()[i], 
					exp.getPosicionesDeDatosInvalidos()[i], exp.getValoresInvalidos()[i]});
		}
				
		String columnas[] = getColumnas();
		
		cmbValidacionesClase.removeAllItems();
		for (int i = 0; i < columnas.length; i++) 
		{
			cmbValidacionesClase.addItem(columnas[i]);
		}
		menu(EXPRESIONES);
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		Object presionado = e.getSource();
		
		if(presionado == btnEliminarC)
		{
			if(modC == false)
				new expresiones().eliminar(nombre);
			mod = true;
			modC = true;
			btnRestaurar.setVisible(true);
			eliminado = tblClase.getSelectedColumn();
			tblClase.getColumnModel().removeColumn(tblClase.getColumnModel().getColumn(eliminado));
			tblClase.updateUI();
			
			archivo.guardar(getColumnas(), getFilas());
			
			String path = "/temp"; 
			URL ruta = this.getClass().getResource(path);
			leerArchivo archivo = new leerArchivo();
			String dir = ruta.getPath()+"/temp.csv";

			actualizarTable(archivo.encabezado(dir), archivo.mostrar(dir), archivo.getRow(dir));
			new expresiones().eliminarExp(getColumnas().length+1,nombre,eliminado);
			
			menu(RESTAURAR);
		}
		else if(presionado == btnEliminarF)
		{	
			btnRestaurar.setVisible(true);
			dtmClase.removeRow(tblClase.getSelectedRow());
			tblClase.repaint();
			
			archivo.guardar(getColumnas(), getFila());
			
			mod = true;
			menu(RESTAURAR);
		}
		else if(presionado == btnRestaurar)
		{
			mod = false;
			modC = false;
			btnRestaurar.setVisible(false);
			leerArchivo archivo = new leerArchivo();
				
			actualizarTable(archivo.encabezado(url), archivo.mostrar(url), archivo.getRow(url));
			archivo.restaurar();
			new expresiones().restaurarExp(nombre);
			menu(RESTAURAR);
		}
		else if(presionado == btnAplicar)
		{
			new expresiones().setExpresiones(getColumnas().length,nombre,cmbValidacionesClase.getSelectedIndex(),txtValidacionesExpReg.getText());
			validateRegExp();
		}
		else if(presionado == btnTransformacionA)
		{
			for (int i = 0; i < fila.length; i++) 
			{
				fila[i] = new transformaciones().getDistanciaMinima(txtTransformacion.getText(),fila[i]);
			}
			
			dtmTransformacion.addColumn(tblClase.getColumnName(cont),fila);
			dialogo.dispose();
		}
		else if(presionado == btnTransformacionC)
		{
			dtmTransformacion.addColumn(tblClase.getColumnName(cont),fila);
			dialogo.dispose();
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) 
	{
		Object seleccionado = e.getSource(); 
		
		if(seleccionado == cmbFrecuencia)
		{
			leerArchivo archivo = new leerArchivo();
			
			if(modificado() == false)
			{
				ArrayList<archivo> list = archivo.valores(url, cmbFrecuencia.getSelectedIndex());
				list.remove(0);
				
				int cont = dtmFrecuencia.getRowCount();
				
				for (int i = 0; i < cont; i++)
				{
					dtmFrecuencia.removeRow(0);
				}
				
				for (archivo ar : list)
				{
					Object[] row={ar.nombre,ar.repeticion}; 
					
					dtmFrecuencia.addRow(row);
				}
			}else
			{
				String path = "/temp";  
				URL ruta = this.getClass().getResource(path);
				
				ArrayList<archivo> list = archivo.valores(ruta.getPath()+"/temp.csv", cmbFrecuencia.getSelectedIndex());
				list.remove(0);
				
				int cont = dtmFrecuencia.getRowCount();
				
				for (int i = 0; i < cont; i++) 
				{
					dtmFrecuencia.removeRow(0);
				}
				
				for (archivo ar : list)
				{
					Object[] row={ar.nombre,ar.repeticion}; 
					
					dtmFrecuencia.addRow(row);
				}
			}
		}
	}
}
